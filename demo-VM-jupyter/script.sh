#! /bin/bash
mkdir /opt/notebooks
su -c '# Install Miniconda
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh
bash ~/miniconda.sh -u -b -p ~/miniconda
eval "$(~/miniconda/bin/conda shell.bash hook)"
source ~/.bashrc
conda init
rm -rf ~/miniconda.sh
# Configure Jupyter for AWS HTTP
conda install jupyter -y --quiet
jupyter notebook --notebook-dir=/opt/notebooks --ip='0.0.0.0' --port=8888 --no-browser --NotebookApp.password='sha1:0fdce5543fee:2a61332f8388de75039601d91ae3b4a9f6047e01'' - ubuntu